package ru.yaleksandrova.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.yaleksandrova.tm.command.AbstractCommand;
import java.util.Collection;

public final class DisplayCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "commands";
    }

    @NotNull
    @Override
    public String arg() {
        return "-cmd";
    }

    @NotNull
    @Override
    public String description() {
        return "Display list commands";
    }

    @Override
    public void execute() {
        @NotNull final Collection<AbstractCommand> commands = serviceLocator.getCommandService().getCommands();
        for (@NotNull final AbstractCommand command : commands)
            System.out.println(command.name());
    }

}
