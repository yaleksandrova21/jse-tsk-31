package ru.yaleksandrova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.yaleksandrova.tm.api.IRepository;
import ru.yaleksandrova.tm.model.User;

import java.util.List;

public interface IUserRepository extends IRepository<User> {

    @NotNull
    User findByLogin(@NotNull String login);

    @NotNull
    User findByEmail(@NotNull String email);

    @NotNull
    User removeUserById(@NotNull String id);

    @NotNull
    User removeUserByLogin(@NotNull String login);

}
