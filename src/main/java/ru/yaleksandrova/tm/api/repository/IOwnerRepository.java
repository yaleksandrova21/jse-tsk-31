package ru.yaleksandrova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.yaleksandrova.tm.api.IRepository;
import ru.yaleksandrova.tm.model.AbstractOwnerEntity;

import java.util.Comparator;
import java.util.List;

public interface IOwnerRepository<E extends AbstractOwnerEntity> extends IRepository<E> {

    void add(@NotNull E entity);

    void remove(@NotNull String userId, @NotNull E entity);

    void clear(@NotNull String userId);

    @NotNull
    List<E> findAll(@NotNull String userId);

    @NotNull
    List<E> findAll(@NotNull String userId, @NotNull Comparator<E> comparator);

    @NotNull
    E findById(@NotNull String userId, @NotNull String id);

    @NotNull
    E findByIndex(@NotNull String userId, @NotNull Integer index);

    @NotNull
    E removeById(@NotNull String userId, @NotNull String id);

    @NotNull
    E removeByIndex(@NotNull String userId, @NotNull Integer index);

    Integer size(String userId);

}

