package ru.yaleksandrova.tm.component;

import lombok.SneakyThrows;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Backup {

    private static final String COMMAND_SAVE = "backup-save";

    private static final String COMMAND_LOAD = "backup-load";

    private static final int INTERVAL = 30;

    private final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

    private final Bootstrap bootstrap;

    public Backup(Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void init() {
        load();
        executorService.scheduleWithFixedDelay(this::save, 0, INTERVAL, TimeUnit.SECONDS);
    }

    @SneakyThrows
    public void save() {
        bootstrap.parseCommand(COMMAND_SAVE);
    }

    @SneakyThrows
    public void load() {
        bootstrap.parseCommand(COMMAND_LOAD);
    }

    public void executorStop() {
        executorService.shutdown();
    }

}
